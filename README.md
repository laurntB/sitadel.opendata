
# sitadel.opendata

<!-- badges: start -->
<!-- badges: end -->

Le but de sitadel.opendata est de pouvoir commencé rapidement sous "R" 
l'analyse des fichiers mis à disposition sur le <https://www.statistiques.developpement-durable.gouv.fr/liste-des-permis-de-construire-et-autres-autorisations-durbanisme>

## Utilisation en mode développement

  1. cloner le dépôt
  2. charger la bibliothèque grâce à `pkgload::load_all('.')`

## Example

Ci-dessous un exemple qui montre l'utilisation basique d'une fonction du package

``` r
library(sitadel.opendata)  # ou  `pkgload::load_all('.')` en mode développement

read_lgts('PC_DP_creant_locaux_2017_2023.csv')
```

