
.table_llpd <- function(afc, reg_filter=NULL){
  # -- représentation
  #
  # @param afc afc 
  # @param reg_filter reg_filter
  #
  # @return tibble
  
  if(!is.null(reg_filter))
    afc <- afc %>% 
      purrr::map(.f=function(.data){
        .data %>% filter(REG == reg_filter)
      })
  
  message("Construction du tableau interne llpd ...")
  wide_table <- dplyr::bind_rows(afc$rlgts,afc$rlocx,afc$rpeam,afc$rdmol)   
  
  cn_rlgts <- colnames(afc$rlgts)
  cn_rlocx <- colnames(afc$rlocx)
  cn_rpeam <- colnames(afc$rpeam)
  cn_rdmol <- colnames(afc$rdmol)
  
  commonCols <- intersect(
    intersect(
      intersect(cn_rlgts,cn_rlocx),
      cn_rpeam
    ),
    cn_rdmol
  )
  
  selcols <- c(
    commonCols,
    "nb.tot",
    "surf.tot",
    "surf.HAB"
  )
  
  narrowed <- wide_table %>% 
    dplyr::select(tidyselect::all_of(selcols))
  
  narrowed %>% 
    mutate(annee.aut = lubridate::date(DATE_REELLE_AUTORISATION) %>% lubridate::year())
}


.explodeby_parcelles <- function(tibble_in)
{
  nr.ori <- nrow(tibble_in)
  nr.dau_uniq <- nrow(tibble_in %>% distinct(TYPE_DAU, NUM_DAU))
  if(nr.ori==nr.dau_uniq)
    message("Attention : problème d'unicité des référence d'urbanisme relevé !")
  parcelles <- tibble_in %>% sanitize_parcelles()
  tibble_in %>% 
    dplyr::left_join(parcelles,by=join_by(TYPE_DAU, NUM_DAU)) %>%
    dplyr::mutate(
      prfx_cadastre = stringr::str_c(DEP,stringr::str_sub(COMM,3,5)),
      geo_parcel = stringr::str_c(prfx_cadastre,parcelle.sec,parcelle.num),
      iddau = stringr::str_c(TYPE_DAU,'-',NUM_DAU)
    )
}






#' Synthèse par parcelle
#'
#' @param afc liste issue de l'invocation de `all_from_csv()`
#' @param reg_filter pris dans les codes officiels géographiques régionaux
#'
#' @return tibble
#' @export
explodeby_parcelles <- function(afc, reg_filter=NULL)
{
  tibble_in <- .table_llpd(afc, reg_filter = reg_filter)
  message("Transformation du tableau avec une ligne par parcelle renseignée ...")
  .explodeby_parcelles(tibble_in)
}