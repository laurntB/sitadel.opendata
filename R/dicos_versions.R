
#' Lister les sépcifications reconnues par la biblithèque
#' @encoding UTF-8
#' @return un liste de date de spécifications
#' @export
#'
#' @examples
#' available_dicos_versions()
available_dicos_versions <- function()
{
  names(DICOS_IN_SYSDATA)
}




get_skip_param <- function(version)
{
  pskip <- 0
  if(is.null(version)) 
     version <- sitadel.opendata.GetOption('dicos_version')
  if(version == "20230928") 
    pskip <- 1
  pskip
}




get_encoding <- function(version)
{
  pencoding <- readr::default_locale()
  if(is.null(version)) 
    version <- sitadel.opendata.GetOption('dicos_version')
  if(version == "20220501") 
    pencoding <- readr::locale(encoding="WINDOWS-1252")
  pencoding
}