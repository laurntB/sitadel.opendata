transmute_TYPE_PRINCIP_LOGTS_CREES_to_factor <- function(data, version=NULL){
  
  if(is.null(version)) 
    version <- sitadel.opendata.GetOption('dicos_version')
  
  if(version == "20220501")
  {
    data <- data %>% 
      dplyr::mutate(
        TYPE_PRINCIP_LOGTS_CREES = forcats::fct_recode(
          TYPE_PRINCIP_LOGTS_CREES,
          'ip'  = "1",
          'ig'  = "2",
          'col' = "3",
          'res' = "4"
        )
      )
  }
  
  if(version == "20230928")
  {
    data <- data %>% 
      dplyr::mutate(
        TYPE_PRINCIP_LOGTS_CREES = forcats::fct_recode(
          TYPE_PRINCIP_LOGTS_CREES,
          'ip'  = "un logement individuel",
          'ig'  = "plusieurs logements individuels",
          'col' = "collectif hors résidence",
          'res' = "résidence"
        )
      )
  }
  
  data
}




transmute_ETAT_DAU_to_factor <- function(data, version=NULL){
  
  if(is.null(version)) 
    version <- sitadel.opendata.GetOption('dicos_version')
  
  if(version == "20220501")
  {
    data <- data %>% 
      dplyr::mutate(
        ETAT_DAU = forcats::fct_recode(
          ETAT_DAU,
          'Autorisé'  = "2",
          'Annulé'    = "4",
          'Commencé'  = "5",
          'Terminé'   = "6"
        )
      )
  } else {
    data <- data %>% 
      dplyr::mutate(ETAT_DAU = forcats::fct(ETAT_DAU))
  }
  
  data
}




transmute_ETAT_PD_to_factor <- function(data, version=NULL){
  
  if(is.null(version)) 
    version <- sitadel.opendata.GetOption('dicos_version')
  
  if(version == "20220501")
  {
    data <- data %>% 
      dplyr::mutate(
        ETAT_PD = forcats::fct_recode(
          ETAT_PD,
          'Autorisé'  = "2",
          'Annulé'    = "4",
          'Commencé'  = "5",
          'Terminé'   = "6"
        )
      )
  } else {
    data <- data %>% 
      dplyr::mutate(ETAT_PD = forcats::fct(ETAT_PD))
  }
  
  data
}




transmute_ETAT_PA_to_factor <- function(data, version=NULL){
  
  if(is.null(version)) 
    version <- sitadel.opendata.GetOption('dicos_version')
  
  if(version == "20220501")
  {
    data <- data %>% 
      dplyr::mutate(
        ETAT_PA = forcats::fct_recode(
          ETAT_PA,
          'Autorisé'  = "2",
          'Annulé'    = "4",
          'Commencé'  = "5",
          'Terminé'   = "6"
        )
      )
  } else {
    data <- data %>% 
      dplyr::mutate(ETAT_PA = forcats::fct(ETAT_PA))
  }
  
  data
}






transmute_RES_PRINCIP_OU_SECOND_to_factor <- function(data, version=NULL){
  
  if(is.null(version)) 
    version <- sitadel.opendata.GetOption('dicos_version')
  
  if(version == "20220501")
  {
    data <- data %>% 
      dplyr::mutate(
        RES_PRINCIP_OU_SECOND = forcats::fct_recode(
          RES_PRINCIP_OU_SECOND,
          'résidence principale'  = "1",
          'non rempli'            = "2",
          'résidence secondaire'  = "3"
        )
      )
  } else {
    data <- data %>% 
      dplyr::mutate(RES_PRINCIP_OU_SECOND = forcats::fct(RES_PRINCIP_OU_SECOND))
  }
  
  data
}



transmute_NATURE_PROJET_DECLAREE_to_factor <- function(data, version=NULL){
  
  if(is.null(version)) 
    version <- sitadel.opendata.GetOption('dicos_version')
  
  if(version == "20220501")
  {
    data <- data %>% 
      dplyr::mutate(
        NATURE_PROJET_DECLAREE = forcats::fct_recode(
          NATURE_PROJET_DECLAREE,
          'nouvelle construction'              = "1",
          'travaux sur construction existante' = "2"
        )
      )
  } else {
    data <- data %>% 
      dplyr::mutate(NATURE_PROJET_DECLAREE = forcats::fct(NATURE_PROJET_DECLAREE))
  }
  
  data
}

