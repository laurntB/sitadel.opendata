
#' Indicateurs annuels sur le logement
#'
#' @param rlgts répertoire des AU sur le domaine logements
#' @param etat valeurs admissibles : "AUT" ou "COM
#'
#' @return tsibble
#' 
#' @export
#'
xitsa_rlgts_indcol <- function(rlgts,etat="AUT")
{
  rlgts %>% 
    xitsm_rlgts_indcol(etat=etat) %>% 
    tsibble::group_by_key() %>% 
    tsibble::index_by(annee = lubridate::year(ym)) %>% 
    dplyr::summarize(
      nbr = sum(nbr)
    ) %>% 
    dplyr::mutate(
      annee = as.integer(annee)
    ) %>% 
    dplyr::select(annee, category, nbr)
}
